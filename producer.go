package kafkawriter

import (
	"time"

	kafka "github.com/segmentio/kafka-go"
)

// Producer struct
type Producer struct {
	*kafka.Writer
}

// New Producer
func New(URL, topic string, delay time.Duration) *Producer {
	return &Producer{createWriter(URL, topic, delay)}
}

func createWriter(URL, topic string, delay time.Duration) *kafka.Writer {
	return kafka.NewWriter(kafka.WriterConfig{
		Brokers:      []string{URL},
		Topic:        topic,
		Balancer:     &kafka.LeastBytes{},
		BatchTimeout: delay * time.Millisecond,
	})
}
